import java.io.IOException;
import java.util.Scanner;
import java.lang.Character;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UtilKit {

    static boolean isNumber(String string){
        int d;
        try
        {
            d = Integer.parseInt(string);
            if (d < 0){
                System.out.println("INPUT IS NEGATIVE NUMBER.");
                return false;
            }else if (d == 0){
                System.out.println("INPUT IS ZERO.");
                return false;
            }else if(string.isEmpty()){
                return false;
            }
        }
        catch(NumberFormatException nfe)
        {
            if(string.isEmpty()) {return false;}
            System.out.println("INPUT IS INVALID.");
            return false;
        }
        return true;
//        return (d > 0)?true:false;
    }

    static boolean isNumberFrom0(String string){
        int d;
        try
        {
            d = Integer.parseInt(string);
            if (d < 0){
                System.out.println("INPUT IS NEGATIVE NUMBER.");
                return false;
            }else if(string.isEmpty()){
                return false;
            }
        }
        catch(NumberFormatException nfe)
        {
            System.out.println("INPUT IS INVALID.");
            return false;
        }
        return true;
//        return (d >= 0)?true:false;
    }



    public static void promptEnterKey(){
        System.out.println("\nPress \"ENTER\" to continue...");
        try{  System.in.read();}catch(Exception e){	e.printStackTrace();}
        System.out.println("\n\n");
    }

    public static boolean containsSpecialCharacter(String str) {

        Pattern pattern = Pattern.compile("[^®a-zA-Z ]");


//        for (int i = 0; i < str.length(); ++i){
//            char ch = str.charAt(i);
//            if (!Character.isDigit(ch) && !Character.isLetter(ch)) {
//                return false;
//            } else if (Character.isDigit(ch)) {
//                return false;
//            }else {
//                return true;
//            }
//
//        }

        Matcher matcher = pattern.matcher(str);
        if(matcher.find()){
            return false;
        }else if(str=="" | str.isEmpty()){
            return false;
        }


        return true;
    }


    public static int inputNumber(String label) {

        Scanner scanner = new Scanner(System.in);

        String temp;
        do {
            System.out.print(label);
            temp = scanner.nextLine();
        } while (!UtilKit.isNumber(temp));

        return Integer.parseInt(temp);

    }

    public static String inputString(String label){
        Scanner scanner = new Scanner(System.in);
        String temp;
        System.out.print(label);
        temp = scanner.nextLine();
        return temp;
    }

}

public class Constants {

    static final String SETUP_HOTEL             = "\n------- Setting up hotel -------";
    static final String ENTER_NUMBER_FLOOR      = "-> Enter number of floors : ";
    static final String ENTER_NUMBER_ROOM       = "-> Enter number of rooms in each floors : ";
    static final String ENTER_GUEST_NAME_TO_SEARCH        = "-> enter guest's name to search : ";
    static final String RESULT_OF_SEARCHING     = "\n=> Result of searching : ";
    static String SETUP_HOTEL_SUCCESSFUL(int floor, int rooms){
        return "=> Hotel is already setup with " + floor + " floors and "+ rooms * floor +" rooms successfully.\n";
    }

    static String RESULT_SEARCHING_GUEST_NAME(int floor, int room, String guestName) {
        return "Guest's Name : '" + guestName + "' is in Room : '" + room + "' On Floor : '" + floor + "' ";
    }

    static final String HOTEL_MANAGEMENT_SYSTEM = "\n------- Hotel Management System -------";
    static final String CHECK_IN                = "1- Check In";
    static final String CHECK_OUT               = "2- Checkout";
    static final String DISPLAY                 = "3- Display";
    static final String SEARCH_QUEST_NAME       = "4- Search Guest's Name";
    static final String EXIT                    = "5- Exit";
    static final String LONG_LINE               = "-----------------------------------------";
    static final String CHOOSE_OPTION           = "-> Choose option(1-5) : ";

    static final String DISPLAY_HOTEL_INFO      = "\n---------- Display hotel information ----------";
    static final String CHECK_IN_HOTEL          = "\n---------- Chick in hotel ----------";
    static String ENTER_FLOOR_NUMBER(int floor){
        return floor==1 ? "-> Enter floor number(only 1 floor) :" : "-> Enter floor number(1-"+floor+") :";
    }
    static String ENTER_ROOM_NUMBER(int floor){
        return floor==1 ? "-> Enter room number(only 1 room) :" : "-> Enter room number(1-"+floor+") :";
    }
    static final String ENTER_GUEST_NAME        = "-> Enter guest's name :";

    static String CHECK_IN_SUCCESSFUL(String name){
        return "=> "+name+" check in successfully!";
    }

    static final String ALREADY_CHECK_IN        = "-> This room is already check in, Please find another room!";
    static final String CHECK_OUT_HOTEL         = "\n---------- Checkout from hotel ----------";
    static String CHECK_OUT_CONFIRM(String name){
        return "-> Guest's Name: "+name+", Press 1 to checkout and 0 to cancel: ";
    }

    static String CHECK_OUT_SUCCESSFUL(String name){
        return "=> "+name+" has been checkout successfully!";
    }

    static final String NO_ONE_CHECK_IN         = "=> No one checkin in this room, you can't checkout!\n";
    static final String GOODBYE                 = "-> Good bye!";
    static final String ALERT_INPUT_NUMBER_1_TO_2      = "Please input number 1 to 4";


    static final String SEARCH_GUEST         = "\n---------- Search Guest's Name ----------";

}

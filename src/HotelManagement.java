import java.util.Scanner;

public class HotelManagement {

    private String floorAndRooms[][];

    void setupHotel() {
        System.out.println(Constants.SETUP_HOTEL);
        int floors = UtilKit.inputNumber(Constants.ENTER_NUMBER_FLOOR);
        int rooms = UtilKit.inputNumber(Constants.ENTER_NUMBER_ROOM);
        floorAndRooms = new String[floors][rooms];
        System.out.println(Constants.SETUP_HOTEL_SUCCESSFUL(floors, rooms));
    }

    void displayHotel() {
        System.out.println(Constants.HOTEL_MANAGEMENT_SYSTEM);
        System.out.println(Constants.CHECK_IN);
        System.out.println(Constants.CHECK_OUT);
        System.out.println(Constants.DISPLAY);
        System.out.println(Constants.SEARCH_QUEST_NAME);
        System.out.println(Constants.EXIT);
        System.out.println(Constants.LONG_LINE);
        chooseMenu();
    }

    private void chooseMenu() {
        int chooseNumber = UtilKit.inputNumber(Constants.CHOOSE_OPTION);
        switch (chooseNumber) {
            case 1:
                checkIn();
                displayHotel();
                break;
            case 2:
                checkOut();
                displayHotel();
                break;
            case 3:
                displayList();
                displayHotel();
                break;
            case 4:
                searchGuestName();
                displayHotel();
            case 5:
                    System.out.println(Constants.GOODBYE);
                System.exit(0);
                break;
            default:
                System.out.println(Constants.ALERT_INPUT_NUMBER_1_TO_2);
                chooseMenu();
                break;

        }
    }

    private void checkIn(){
        int tempFloor;
        int tempRoom;
        String guessName;

        System.out.println(Constants.CHECK_IN_HOTEL);
        do {
            tempFloor = UtilKit.inputNumber(Constants.ENTER_FLOOR_NUMBER(floorAndRooms.length));
        }while(tempFloor>floorAndRooms.length);

        do {
            tempRoom = UtilKit.inputNumber(Constants.ENTER_ROOM_NUMBER(floorAndRooms[0].length));
        }while(tempRoom>floorAndRooms[0].length);

        if(floorAndRooms[tempFloor-1][tempRoom-1] != null) {
            System.out.println(Constants.ALREADY_CHECK_IN);
            displayHotel();
        }

        do {
            guessName = UtilKit.inputString(Constants.ENTER_GUEST_NAME);

        }while(!UtilKit.containsSpecialCharacter(guessName));
        floorAndRooms[tempFloor-1][tempRoom-1] = guessName;
        System.out.println(Constants.CHECK_IN_SUCCESSFUL(guessName));
        UtilKit.promptEnterKey();

    }

    private void checkOut(){
        int tempFloor;
        int tempRoom;
        int choice;
        System.out.println(Constants.CHECK_OUT_HOTEL);
        do {
            tempFloor = UtilKit.inputNumber(Constants.ENTER_FLOOR_NUMBER(floorAndRooms.length));
        }while(tempFloor>floorAndRooms.length);

        do {
            tempRoom = UtilKit.inputNumber(Constants.ENTER_ROOM_NUMBER(floorAndRooms[0].length));
        }while(tempRoom>floorAndRooms[0].length);

        if(floorAndRooms[tempFloor-1][tempRoom-1] != null) {
            do{
                Scanner scanner = new Scanner(System.in);
                String temp;
                do {
                    System.out.print(Constants.CHECK_OUT_CONFIRM(floorAndRooms[tempFloor-1][tempRoom-1]));
                    temp = scanner.next();
                } while (!UtilKit.isNumberFrom0(temp));

                choice =  Integer.parseInt(temp);
            }while(!(choice == 0 || choice == 1));

            if (choice == 1){
                System.out.println(Constants.CHECK_OUT_SUCCESSFUL(floorAndRooms[tempFloor-1][tempRoom-1]));
                floorAndRooms[tempFloor-1][tempRoom-1] = null;
                UtilKit.promptEnterKey();
                displayHotel();

            }else{
                displayHotel();
            }


        }else{
            System.out.println(Constants.NO_ONE_CHECK_IN);
            UtilKit.promptEnterKey();
            displayHotel();
        }

    }
    private void searchGuestName() {
        String search;
        boolean isFound = false;

        System.out.println(Constants.SEARCH_GUEST);
        do {
            search = UtilKit.inputString(Constants.ENTER_GUEST_NAME_TO_SEARCH);

        }while(!UtilKit.containsSpecialCharacter(search));

        System.out.println(Constants.RESULT_OF_SEARCHING);
        for(int i=1; i<floorAndRooms.length+1; i++) {

            for (int j = 0; j < floorAndRooms[i-1].length; j++) {
                if(search.equalsIgnoreCase(floorAndRooms[i-1][j])) {
                    isFound = true;
                    System.out.println(Constants.RESULT_SEARCHING_GUEST_NAME(i, j+i, floorAndRooms[i-1][j]));
                }
            }

        }
        if (!isFound) {
            System.out.println("Guest : " + search + " Not Stay Here");
        }
        System.out.println();
    }

    private void displayList(){
        System.out.println("\n\n"+Constants.DISPLAY_HOTEL_INFO);
        for (int i = 0; i < floorAndRooms.length; i++){
            System.out.print("Floor "+ (i+1) + " : ");
            for(int a = 0; a < floorAndRooms[0].length; a++){
                System.out.print("\t" + floorAndRooms[i][a] + "\t");
            }
            System.out.println();

        }

        UtilKit.promptEnterKey();
    }


}

